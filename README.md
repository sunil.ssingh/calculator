# Simple Calculator using Unity project
:point_right: Unity 2017.3.0p4

Open Caclualtor scene and enter the expression, this can eval simple and nested math expression.


**Info**
- Check Calulator/config folder for operator and engine configuration.
- In calulator scene look for *CalcCanvas* game object which is the main entry point. All the required depdent scripts are child to this.

**Feature**

- All operator are scriptiable object which make it easier to configure and add new one (TODO via reflection or Lamda expression).


**Bug**

- Rgex used for validating the input filed which result in ignoring the unary  operato e.g (3 - 5 = -2 but input will ignore '-' sign so it become 3 - 5 = 2)

**TODO**

- Support for additional opetator def via reflection or lamda expression
- Implement unary  operator (e.g -4 + 5  expression on supported) 

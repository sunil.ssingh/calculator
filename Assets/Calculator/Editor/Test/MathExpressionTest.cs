﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;

namespace Octupied.Calc.Api {
    public class MathExpressionTest {

        [Test]
        public void SimpleMathExpression() {
            CalculatorConfigSO config = GetSoInstance<CalculatorConfigSO>();

            var res = MathExpressionParser.Parse("6 + 8", config.Operator).Evaluate();
            Debug.Log("6 + 8 =" + res);
            Assert.AreEqual(res, 14);

            var res1 = MathExpressionParser.Parse("10 * 20 / 50", config.Operator).Evaluate();
            Debug.Log("10 * 20 / 50 =" + res1);
            Assert.AreEqual(res1, 4);

            res1 = MathExpressionParser.Parse("10 + 2 * 6", config.Operator).Evaluate();
            Debug.Log("10 + 2 * 6 =" + res1);
            Assert.AreEqual(res1, 72);

            res1 = MathExpressionParser.Parse("100 * 2 + 12", config.Operator).Evaluate();
            Debug.Log("100 * 2 + 12 =" + res1);
            Assert.AreEqual(res1, 212);

            res1 = MathExpressionParser.Parse("100 * ( 2 + 12 )", config.Operator).Evaluate();
            Debug.Log("100 * ( 2 + 12 ) =" + res1);
            Assert.AreEqual(res1, 1400);
        }

        [Test]
        public void UniaryeMathExpression() {
            CalculatorConfigSO config = GetSoInstance<CalculatorConfigSO>();
            var res = MathExpressionParser.Parse("5 - 8", config.Operator).Evaluate();

            Assert.AreEqual(res, -3);

        }

        [Test(Description = "Unit test for nested operation")]
        public void NestedOrderOfOperation() {

            CalculatorConfigSO config = GetSoInstance<CalculatorConfigSO>();
            var res1 = MathExpressionParser.Parse("((10 + 20) / 3) * 30", config.Operator).Evaluate();
            Assert.AreEqual(res1, 300);

            res1 = MathExpressionParser.Parse("(4+3)*2/5", config.Operator).Evaluate();
            Assert.AreEqual(res1, 2.8);

            res1 = MathExpressionParser.Parse("((12+9)/2)*5", config.Operator).Evaluate();
            Assert.AreEqual(res1, 52.5);
        }


        // A UnityTest behaves like a coroutine in PlayMode
        // and allows you to yield null to skip a frame in EditMode
        [UnityTest]
        public IEnumerator MathExpressionTestWithEnumeratorPasses() {
            // Use the Assert class to test conditions.
            // yield to skip a frame
            yield return null;
        }

        public static T GetSoInstance<T>() where T : ScriptableObject {
            //FindAssets uses tags check documentation for more info
            string[] guids = AssetDatabase.FindAssets("t:" + typeof(T).Name);  
            foreach(var guid in guids)
            {
                string path = AssetDatabase.GUIDToAssetPath(guid);
                return AssetDatabase.LoadAssetAtPath<T>(path);
            }

            return null;
        }
    }
}

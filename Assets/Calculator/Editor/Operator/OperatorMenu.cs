﻿using Octupied.Calc;
using UnityEditor;

namespace Octupied.Ed {

    public static class OperatorMenu {

        //[MenuItem("Assets/Operator/Add")]
        //[MenuItem("Operator/Add")]
        //public static void CreateAddOperatorSO() {
        //    ScriptableObjectHelper.CreateAsset<AddOperator>();
        //}

        //[MenuItem("Assets/Operator/Subtract")]
        //[MenuItem("Operator/Subtract")]
        //public static void CreateSubOperatorSO() {
        //    ScriptableObjectHelper.CreateAsset<SubtractionOperator>();
        //}

        //[MenuItem("Assets/Operator/Multiplication")]
        //[MenuItem("Operator/Multiplication")]
        //public static void CreateMulOperatorSO() {
        //    ScriptableObjectHelper.CreateAsset<MultiplicationOperator>();
        //}

        //[MenuItem("Assets/Operator/Division")]
        //[MenuItem("Operator/Divison")]
        //public static void CreatDivOperatorSO() {
        //    ScriptableObjectHelper.CreateAsset<DivisionOperator>();
        //}

        [MenuItem("Assets/Operator/Create")]
        [MenuItem("Operator/Create")]
        public static void CreateOperatorSO() {
            ScriptableObjectHelper.CreateAsset<OperatorDef>();
        }

        [MenuItem("Assets/Operator/Calc Config")]
        [MenuItem("Operator/Calc Config")]
        public static void CreateCalConfig() {
            ScriptableObjectHelper.CreateAsset<CalculatorConfigSO>();
        }
    }
}
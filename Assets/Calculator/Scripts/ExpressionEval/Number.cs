﻿using Octupied.Calc.Api;

namespace Octupied.Calc {
    public sealed class Number : INode<double> {
        private double Value;
        public Number(double val) {
            Value = val;
        }

        public double Evaluate() {
            return Value;
        }
    }
}

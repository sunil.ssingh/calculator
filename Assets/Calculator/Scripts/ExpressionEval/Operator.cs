﻿using Octupied.Calc.Api;

namespace Octupied.Calc {

    public abstract class Operator : IOperator {

        public int Precedence {
            get;
            private set;
        }

        public string Symbol {
            get;
            private set;
        }

        protected Operator(int precedence, string symbol) {
            Precedence = precedence;
            Symbol = symbol;
        }

        public abstract double Evaluate();
    }
}
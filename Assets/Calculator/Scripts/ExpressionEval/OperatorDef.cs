﻿using Octupied.Calc.Api;
using System;
using UnityEngine;
using INode = Octupied.Calc.Api.INode<double>;

namespace Octupied.Calc {

    public class OperatorDef : ScriptableObject/*, IComparable<OperatorDef>*/ {
        [SerializeField]
        private int _Precedence = 1;
        public int Precedence {
            get {
                return _Precedence;
            }
        }

        [SerializeField]
        private string _Symbol = "";
        public string Symbol {
            get {
                return _Symbol;
            }
        }

        [SerializeField]
        private bool _IsOperator = true;
        public bool IsOperator {
            get {
                return _IsOperator;
            }
        }

        /// <summary>
        /// TODO create custom editor inspector which list all operator type assembly 
        /// for config, for now user has to enter class name
        /// </summary>
        [SerializeField]
        private string OperatorType = "";
        public Func<INode, INode, IOperator> Creator {
            get;
            private set;
        }

        private void OnEnable() {
            if (string.IsNullOrEmpty(Symbol)) {
                throw new Exception("Operator symbol is not defined");
            }

            if (Creator != null)
                return;

            if (string.IsNullOrEmpty(OperatorType) == false) {
                Type type = Type.GetType("Octupied.Calc." + OperatorType);
                Creator = (lhs, rhs) => (IOperator)Activator.CreateInstance(type, new object[] { lhs, rhs });
            }
            else {
                Creator = OperatorFactory.GetDefaulCreator(Symbol);
            }
        }

        //public int CompareTo(OperatorDef other) {
        //    return Precedence.CompareTo(other.Precedence);
        //}

        /// <summary>
        /// not to be used , unity doest not like constructor of its own type
        /// </summary>
        /// <param name="precedence"></param>
        /// <param name="symbol"></param>
        /// <param name="creator"></param>
        //public OperatorDef(int precedence, string symbol, Func<INode, INode, IOperator> creator) {
        //    _Precedence = precedence;
        //    _Symbol = symbol;
        //    Creator = creator;
        //}
    }
}
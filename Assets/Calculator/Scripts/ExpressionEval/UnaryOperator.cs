﻿using Octupied.Calc.Api;
using System;

namespace Octupied.Calc {

    internal class UnaryOperator : INode<double> {
        private INode<double> RightOperand;
        private Func<double, double> Operation;

        public UnaryOperator(INode<double> rhs, Func<double, double> op) {
            RightOperand = rhs;
            Operation = op;
        }

        public double Evaluate() {
            return Operation(RightOperand.Evaluate());
        }
    }
}
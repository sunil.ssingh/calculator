﻿using Octupied.Calc.Api;
using System;
using System.Collections.Generic;
using System.IO;
using INode = Octupied.Calc.Api.INode<double>;

namespace Octupied.Calc {

    public class MathExpressionParser {
        private Tokenizer TokenGenerator;
        private OperatorFactory OperatorFactory;

        public MathExpressionParser(string expression, OperatorFactory factory):
                                    this(new Tokenizer(expression, factory), factory) 
        {
        }

        public MathExpressionParser(Tokenizer tokenizer, OperatorFactory factory) {
            TokenGenerator = tokenizer;
            OperatorFactory = factory;
        }

        /// <summary>
        /// Start injected math expression parsing
        /// </summary>
        /// <returns></returns>
        public INode<double> Parse() {
            var root = ParseExpression();
            if (TokenGenerator.CurrentToken != Token.EOF) {
                throw new InvalidProgramException("Math expression failed to complete!");
            }

            return root;
        }

        /// <summary>
        /// Recursive method which  parse the math expression as per the priority,
        /// operation factory should  evaluate as per precedence of operator
        /// </summary>
        /// <returns>tree left node when there is no more operator to process</returns>
        private INode<double> ParseExpression() {
            var lhs = ParseLeaf();
            while (true) {

                if (TokenGenerator.IsOperator == false)
                    return lhs;
                var opSymbol = TokenGenerator.OperatorSymbol;
                TokenGenerator.Next();
                var rhs = ParseLeaf();
                lhs = OperatorFactory.Create(opSymbol, lhs, rhs);
            }
        }

        private INode ParseHigherPrecedence() {
            var lhs = ParseLeaf();
            while (true) {
                if (TokenGenerator.IsOperator == false )
                    return lhs;

                var opSymbol = TokenGenerator.OperatorSymbol;
                if (OperatorFactory.GetOperatorPrecedence(opSymbol) < 2) {
                    return lhs;
                }
                TokenGenerator.Next();
                var rhs = ParseLeaf();
                lhs = OperatorFactory.Create(opSymbol, lhs, rhs);
            }
        }

        /// <summary>
        /// Parse the leaf node of expression
        /// </summary>
        /// <returns></returns>
        ///<exception cref="InvalidOperationException">If Parentheses not in pair/incomplete</exception>
        private INode<double> ParseLeaf() {
            if (TokenGenerator.CurrentToken == Token.Operand) {
                var node = new Number(TokenGenerator.Number);
                TokenGenerator.Next();
                return node;
            }

            if (TokenGenerator.CurrentToken == Token.OpenParentheses) {
                TokenGenerator.Next();

                var node = ParseExpression();

                if (TokenGenerator.CurrentToken != Token.CloseParentheses)
                    throw new InvalidOperationException("Missing close parenthesis!");

                TokenGenerator.Next();
                return node;
            }

            //unknown token found
            throw new InvalidOperationException($"Unexpected token: {TokenGenerator.CurrentToken}");
        }

        #region static helper
        public static INode<double> Parse(string expression) {
            return Parse(new Tokenizer(new StringReader(expression), OperatorFactory.GetDefault()));
        }

        public static INode Parse(Tokenizer tokenizer) {
            var parser = new MathExpressionParser(tokenizer, OperatorFactory.GetDefault());
            return parser.Parse();
        }

        public static INode<double> Parse(string expression, IEnumerable<OperatorDef> ops) {
            OperatorFactory factory = new OperatorFactory(ops);
            var parser = new MathExpressionParser(new Tokenizer(new StringReader(expression), factory), factory);
            return parser.Parse();
        }
        public static INode<double> Parse(string expression, OperatorFactory factory ) {
            var parser = new MathExpressionParser(new Tokenizer(new StringReader(expression), factory), factory);
            return parser.Parse();
        }
        #endregion
    }
}
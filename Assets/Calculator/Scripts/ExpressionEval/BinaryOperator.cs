﻿using Octupied.Calc.Api;

namespace Octupied.Calc {
    public abstract class BinaryOperator : Operator {
        protected readonly INode<double> LeftOperand;
        protected readonly INode<double> RightOperand;
        protected BinaryOperator(INode<double> lhs, INode<double> rhs, string symbol) :
            this(lhs, rhs, 0, symbol) {
            LeftOperand = lhs;
            RightOperand = rhs;
        }
        protected BinaryOperator(INode<double> lhs, INode<double> rhs, int precedence,
                                string symbol) : base(precedence, symbol) {
            LeftOperand = lhs;
            RightOperand = rhs;
        }
    }
}

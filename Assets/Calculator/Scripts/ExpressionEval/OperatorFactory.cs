﻿using Octupied.Calc.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using INode = Octupied.Calc.Api.INode<double>;

namespace Octupied.Calc {
    public class OperatorFactory {
        private IEnumerable<OperatorDef> Operators;

        public OperatorFactory(IEnumerable<OperatorDef> supportedOps) {
            Operators = supportedOps;
        }

        public bool IsOperator(char symbol) {
            //string supportedOperator = "+-/*";
            //return supportedOperator.Contains(symbol));

            var op = GetOperator(symbol);
            return op != null && op.IsOperator;
        }

        public IOperator Create(char symbol, INode<double> leftOperand, INode<double> rightOperand) {
            OperatorDef op = GetOperator(symbol);
            if (op != null) {
                return op.Creator(leftOperand, rightOperand);
            }

            throw new Exception($"Non supported operator: {symbol}");
        }

        public int GetOperatorPrecedence(char symbol) {
            var opSym = GetOperator(symbol);
            if (opSym == null)
                throw new Exception($"No operator of type found: {symbol}");

            return opSym.Precedence;
        }

        private OperatorDef GetOperator(char symbol) {
            string symbolString = symbol.ToString();
            return Operators.FirstOrDefault(op => op.Symbol == symbolString);
        }

        private string CharToString(char symbol) {
            return string.Empty + symbol;
        }

        public static OperatorFactory GetDefault() {
            //List<OperatorDef> operators = new List<OperatorDef>() {
            //    new OperatorDef(1, "+", (lhs, rhs) => new AddOperator(lhs, rhs)),
            //    new OperatorDef(1, "-", (lhs, rhs) => new SubtractionOperator(lhs, rhs)),
            //    new OperatorDef(3, "*", (lhs, rhs) => new MultiplicationOperator(lhs, rhs)),
            //    new OperatorDef(3, "/", (lhs, rhs) => new DivisionOperator(lhs, rhs)),
            //};

            //return new OperatorFactory(operators);
            throw new Exception("not supported in unity version");
        }

        public static Func<INode, INode, IOperator> GetDefaulCreator(string symbol) {
            if (symbol == "+")
                return (lhs, rhs) => new AddOperator(lhs, rhs);
            else if (symbol == "-")
                return (lhs, rhs) => new SubtractionOperator(lhs, rhs);
            else if (symbol == "*")
                return (lhs, rhs) => new MultiplicationOperator(lhs, rhs);
            else if (symbol == "/")
                return (lhs, rhs) => new DivisionOperator(lhs, rhs);

            //throw new Exception(message: $"Don't know symbol: {symbol} ");
            return null;
        }
    }
}
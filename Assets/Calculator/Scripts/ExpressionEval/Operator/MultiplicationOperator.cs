﻿using Octupied.Calc.Api;

namespace Octupied.Calc {

    public sealed class MultiplicationOperator : BinaryOperator {
        public MultiplicationOperator(INode<double> lhs, INode<double> rhs) : base(lhs, rhs, 3, "*") {
        }

        public override double Evaluate() {
            return LeftOperand.Evaluate() * RightOperand.Evaluate();
        }
    }
}
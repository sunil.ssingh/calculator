﻿using Octupied.Calc.Api;

namespace Octupied.Calc {

    public sealed class SubtractionOperator : BinaryOperator {
        public SubtractionOperator(INode<double> lhs, INode<double> rhs) : base(lhs, rhs, 1, "-") {
        }

        public override double Evaluate() {
            return LeftOperand.Evaluate() - RightOperand.Evaluate();
        }
    }
}
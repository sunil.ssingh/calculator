﻿using System;
using System.Globalization;
using System.IO;
using System.Text;

namespace Octupied.Calc {

    public class Token {
        public static readonly Token EOF = new Token('\0');
        public static readonly Token Operator = new Token('o');
        public static readonly Token Operand = new Token('n');
        public static readonly Token OpenParentheses = new Token('(');
        public static readonly Token CloseParentheses = new Token(')');
        public static readonly Token Comma = new Token(')');
        public char Symbol;

        public Token(char ch) {
            Symbol = ch;
        }
    }

    public class Tokenizer {
        private TextReader Reader;
        private OperatorFactory OPFactory;

        public char CurrentSymbol {
            get;
            private set;
        }

        public CultureInfo Culture {
            get; set;
        }

        public double Number {
            get;
            private set;
        }

        public Token CurrentToken {
            get;
            private set;
        }

        public Char OperatorSymbol {
            get;
            private set;
        }

        public bool IsOperator => CurrentToken == Token.Operator;

        public Tokenizer(string expression, OperatorFactory opFactory) : this(new StringReader(expression), opFactory) {
        }

        public Tokenizer(TextReader reader, OperatorFactory opFactory) {
            Reader = reader;
            Culture = CultureInfo.InvariantCulture;
            OPFactory = opFactory;
            NextSymbol();
            Next();
        }

        /// <summary>
        /// read the char from reader
        /// </summary>
        private void NextSymbol() {
            int ch = Reader.Read();
            CurrentSymbol = ch < 0 ? Token.EOF.Symbol : (char)ch;
        }

        /// <summary>
        /// Get the next token
        /// </summary>
        /// <returns></returns>
        public Token Next() {
            while (char.IsWhiteSpace(CurrentSymbol)) {
                NextSymbol();
            }

            if (CurrentSymbol == '\0') {
                CurrentToken = Token.EOF;
                return CurrentToken;
            }

            if (OPFactory.IsOperator(CurrentSymbol)) {
                OperatorSymbol = CurrentSymbol;
                NextSymbol();
                return CurrentToken = Token.Operator;
            }
            else if (CurrentSymbol == '(') {
                NextSymbol();
                return CurrentToken = Token.OpenParentheses;
            }
            else if (CurrentSymbol == ')') {
                NextSymbol();
                return CurrentToken = Token.CloseParentheses;
            }
            else if (CurrentSymbol == ',') {
                NextSymbol();
                return CurrentToken = Token.Comma;
            }

            TryReadOperand();
            return CurrentToken;
        }

        private void TryReadOperand() {
            var decimalSeparator = Culture.NumberFormat.NumberDecimalSeparator[0];
            var groupSeparator = Culture.NumberFormat.NumberGroupSeparator[0];

            if (char.IsDigit(CurrentSymbol) == false && CurrentSymbol != decimalSeparator)
                return;

            StringBuilder numericBuilder = new StringBuilder();
            do {
                numericBuilder.Append(CurrentSymbol);
                NextSymbol();
            }
            while (char.IsDigit(CurrentSymbol) || CurrentSymbol == decimalSeparator || CurrentSymbol == groupSeparator);

            Number = double.Parse(numericBuilder.ToString(), Culture);
            CurrentToken = Token.Operand;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Octupied.Calc {

    [RequireComponent(typeof(GridLayoutGroup))]
    public class ExpressionHistory : GridButtonSpawner {

        [SerializeField]
        private int HistoryToKeep = 100;

        private List<Button> HistoryExpression = null;
        private int UsedIndex = 0;

        public override void Spawn(Action<string> buttonCallback) {
            HistoryExpression = new List<Button>(HistoryToKeep);
            base.Spawn(buttonCallback);
        }

        public void RegisterExpression(string expression) {
            Button button = null; 
            if (HistoryExpression.Count <= HistoryToKeep) {
                button = Get();
                HistoryExpression.Add(button);
            }

            if (button == null) {
                UsedIndex = UsedIndex % HistoryToKeep;
                button = HistoryExpression[UsedIndex++];
                print("Recycle = " + UsedIndex);
            }
            button.GetComponentInChildren<Text>().text = expression;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Octupied.Calc {

    public class Calculator : MonoBehaviour {
        [SerializeField]
        private CalculatorConfigSO Config = null;
        [SerializeField]
        private OperatorSpawner OpSpawner = null;
        [SerializeField]
        private NumberButtonSpanwer NumberSpanwer = null;
        [SerializeField]
        private InputField InputField = null;
        [SerializeField]
        private ExpressionHistory History = null;

        private void Awake() {
            if (Config == null || OpSpawner == null || NumberSpanwer == null || InputField == null)
                throw new Exception("Calculator Config reference missing");
            CalculatorEngine.Instance.SetOperators(Config.Operator);
        }
        private void Start() {
            List<Button> operatorButtons = new List<Button>();
            OpSpawner.Spawn(Config.Operator, OnButtonPressed, ref operatorButtons);
            NumberSpanwer.Spawn(OnButtonPressed);
            RegisterForSubmit(operatorButtons);
            if (History != null) {
                History.Spawn(OnExpressionSelected);
            }
        }

        private void OnExpressionSelected(string expression) {
            InputField.text = expression.Split(' ')[0];
        }

        private void RegisterForSubmit(List<Button> buttons) {
            Button submit = buttons.Find((btn) => {
                return btn.GetComponentInChildren<Text>().text == "=";
            });
            if (submit != null) {
                submit.onClick.AddListener(() => EvalExpression());
            }
        }

        private void OnButtonPressed(string text) {
            InputField.text += text;
        }

        private void Update() {
            MakeInputActive();
            if (Input.GetKeyUp(KeyCode.Escape)) {
                InputField.text = "";
            }

            if (Input.GetKeyUp(KeyCode.Return)) {
                EvalExpression();
            }
        }

        private void MakeInputActive() {
            InputField.Select();
            InputField.ActivateInputField();
        }

        private void EvalExpression() {
            var expression = InputField.text;
            var result = CalculatorEngine.Instance.Eval(InputField.text);
            InputField.text = result.ToString();
            if (History != null) {
                History.RegisterExpression(expression + " = " + result);
            }
            //print("expression= " + expression + "result= " + InputField.text);
        }
    }
}
﻿using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

namespace Octupied.Calc {

    [RequireComponent(typeof(InputField))]
    public class InputValidator : MonoBehaviour {

        [SerializeField]
        private string SupportedChar = "^[0-9+*-/()., ]+$";
        [SerializeField]
        private FlashWrong Warning = null;

        private InputField TextField = null;
        private Regex AllowedCharRegex = null;

        private void Awake() {
            TextField = GetComponent<InputField>();
            AllowedCharRegex = new Regex(SupportedChar);
            TextField.onValidateInput += TextValidation;
        }

        private char TextValidation(string text, int charIndex, char addedChar) {
            //print(" text= " + text + " char= " + addedChar);
            bool isOperatorRepeat = CalculatorEngine.Instance.IsOperator(addedChar);
            isOperatorRepeat &= (CalculatorEngine.Instance.IsUnaryOperator(addedChar) == false);
            //check if input start is valid, no support of uniary opeator as of now
            if (text.Length == 0 && isOperatorRepeat) 
            {
                Debug.LogWarning("USer '(, ) or numebric to start the expression'");
                if (Warning != null) {
                    Warning.Flash();
                }
            }
            // check if operator is repeated again, we want  opeator and operand
            if (text.Length > 0) {
                var lastChar = text[text.Length - 1];
                isOperatorRepeat &= CalculatorEngine.Instance.IsOperator(lastChar);
                if (isOperatorRepeat) {
                    Debug.LogWarning($"Operator{lastChar} and {addedChar}! hint, try input some number!");
                    if (Warning != null) {
                        Warning.Flash();
                    }
                }
            }

            if (isOperatorRepeat == false && AllowedCharRegex.IsMatch(addedChar.ToString())) {
                return addedChar;
            }

            return '\0';
        }
    }
}
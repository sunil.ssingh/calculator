﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

namespace Octupied.Calc {

    [RequireComponent(typeof(GridLayoutGroup))]
    public class OperatorSpawner : GridButtonSpawner {
        public void Spawn(IEnumerable<OperatorDef> operators, Action<string> callback, ref List<Button> buttons) {
            base.Spawn(callback);
            foreach (var op in operators) {
                Button operatorUI = Get();
                operatorUI.GetComponentInChildren<Text>().text = op.Symbol;
                operatorUI.name = "op_" + op.Symbol;
                if (buttons != null) {
                    buttons.Add(operatorUI);
                }
            }
        }
    }
}
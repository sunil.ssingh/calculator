﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Octupied.Calc {
    [RequireComponent(typeof(RawImage))]
    public class FlashWrong : MonoBehaviour {
        [SerializeField]
        private float Duration = 2.0f;
        [SerializeField]
        private float AlphaMin = 0.3f;

        private Coroutine Current = null;
        private RawImage Image = null;
        private Color CachedColor;
        private void Awake() {
            Image = GetComponent<RawImage>();
            CachedColor = Image.color;
        }
        [ContextMenu("Flash")]
        public void Flash() {
            if (Current != null)
                StopCoroutine(Current);
            gameObject.SetActive(true);
            Current = StartCoroutine(ChangeAlpha());
        }

        private IEnumerator ChangeAlpha() {
            float endTime = Time.time + Duration;
            float accumTime = 0;
            Image.color = CachedColor;
            while (endTime > Time.time) {
                Color color = Image.color;
                color.a = Mathf.Lerp(color.a, AlphaMin, accumTime);
                accumTime += Time.deltaTime;
                Image.color = color;
                yield return null;
            }
            gameObject.SetActive(false);
        }
    }
}

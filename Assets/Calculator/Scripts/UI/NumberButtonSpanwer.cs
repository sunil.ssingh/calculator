﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Octupied.Calc {

    [RequireComponent(typeof(GridLayoutGroup))]
    public class NumberButtonSpanwer : GridButtonSpawner {

        public override void Spawn(Action<string> buttonCallback) {
            base.Spawn(buttonCallback);
            for (int idx = 0; idx < 9; ++idx) {
                Button number = Get();
                number.name = "Number_" + idx;
                number.GetComponentInChildren<Text>().text = idx.ToString();
            }
        }
    }
}
﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Octupied.Calc {

    [RequireComponent(typeof(GridLayoutGroup))]
    public abstract class GridButtonSpawner : MonoBehaviour {
        [SerializeField]
        private Button Prefab = null;
        [SerializeField]
        private bool OnAwakeSpawn = false;
        private GridLayoutGroup Layout = null;
        private Action<string> ButtonPressCallback;

        // Use this for initialization
        private void Awake() {
            if (Prefab == null)
                throw new MissingReferenceException("Number prefab reference not found!");
            Layout = GetComponent<GridLayoutGroup>();
            if(OnAwakeSpawn)
                Spawn(null);
        }

        protected Button Get() {
            Button button = GameObject.Instantiate(Prefab, Layout.transform);
            button.onClick.AddListener(() => OnButtonClick(button));
            return button;
        }

        private void OnButtonClick(Button button) {
            if (ButtonPressCallback != null) {
                ButtonPressCallback(button.GetComponentInChildren<Text>().text);
            }
        }

        public virtual void Spawn(Action<string> buttonCallback) {
            ButtonPressCallback = buttonCallback;
        }
    }
}
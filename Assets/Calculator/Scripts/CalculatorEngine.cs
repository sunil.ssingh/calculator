﻿using System.Collections.Generic;

namespace Octupied.Calc {

    public class CalculatorEngine {
        public static readonly CalculatorEngine Instance = new CalculatorEngine();
        private OperatorFactory OperatorFactory = null;
        private CalculatorEngine() {
        }

        public void SetOperators(IEnumerable<OperatorDef> ops) {
            foreach (OperatorDef op in ops) {
                UnityEngine.Debug.LogWarning(op.Symbol);
            }
            OperatorFactory = new OperatorFactory(ops);
        }

        public bool IsOperator(char sym) {
            return OperatorFactory.IsOperator(sym);
        }

        public bool IsUnaryOperator(char sym) {
            string symbol = sym.ToString();
            return sym == '-' || symbol == "++" || symbol == "--";
        }

        public double Eval(string expression) {
            return MathExpressionParser.Parse(expression, OperatorFactory).Evaluate();
        }
    }
}
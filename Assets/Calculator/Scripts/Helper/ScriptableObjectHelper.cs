﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class ScriptableObjectHelper {
    public static T CreateAsset<T>() where T : ScriptableObject {
        T asset = ScriptableObject.CreateInstance<T>();
        string path = AssetDatabase.GetAssetPath(Selection.activeObject);
        //Debug.LogWarning("Path " + path);
        if (string.IsNullOrEmpty(path))
            path = "Assets";
        else
            path = path.Replace(Path.GetFileName(path), string.Empty);
        string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + "/New_" + typeof(T).Name + ".asset");
        Debug.LogWarning("Path of SO =" + path + " filename= " + assetPathAndName);
        AssetDatabase.CreateAsset(asset, assetPathAndName);

        AssetDatabase.SaveAssets();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;
        return asset;
    }
}

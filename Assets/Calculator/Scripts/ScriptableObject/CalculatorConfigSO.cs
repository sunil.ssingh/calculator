﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Octupied.Calc {
    public class CalculatorConfigSO : ScriptableObject {
        [SerializeField]
        private OperatorDef[] _Operators = null;

        public IEnumerable<OperatorDef> Operator {
            get {
                return _Operators.OrderByDescending(other => other.Precedence);
            }
        }
    }
}

﻿namespace Octupied.Calc.Api {

    /// <summary>
    ///  Node of expression tree
    /// </summary>
    /// <typeparam name="TType"> evaluate to type  </typeparam>
    public interface INode<TType> {

        /// <summary>
        /// Evaluate the node
        /// </summary>
        /// <returns>result of TType </returns>
        TType Evaluate();
    }
}
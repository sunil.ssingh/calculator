﻿namespace Octupied.Calc.Api {
    /// <summary>
    /// The math operator interface , math operator called by there precedence, 
    /// which will be use to construct the tree
    /// </summary>
    public interface IOperator : INode<double> {
        /// <summary>
        /// Define the priority of operator
        /// </summary>
        int Precedence {
            get;
        }
        /// <summary>
        ///  Symbol which represent the operator e.g 
        ///  '+' => addition
        ///  '*' => Multiplication
        /// used by Tokenizer class to construct the object of operator type
        /// </summary>
        string Symbol {
            get;
        }
    }
}
